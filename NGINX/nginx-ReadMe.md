helm upgrade --install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace

* Install nginx ingress using helm chart
> helm install nginx-ingress ingress-nginx/ingress-nginx -f values-ingress-nginx.yml --namespace nginx-ns --create-namespace

> helm upgrade nginx-ingress nginx-ingress/nginx-ingress -f values-ingress-nginx.yml --namespace nginx-ns

* Verify sample blue green deployment
kubectl apply -f .\sample-blue-green-nginx.yaml

* Verify it is working using: 
https://wso.cloudscale.in/menu