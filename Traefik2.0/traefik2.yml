kind: Namespace
apiVersion: v1
metadata:
  name: traefik2
  labels:
    name: traefik2
---
# minimum TLS1.2
apiVersion: traefik.containo.us/v1alpha1
kind: TLSOption
metadata:
  name: mintls12
  namespace: traefik2
spec:
  minVersion: VersionTLS12
---
#minimum TLS1.3
apiVersion: traefik.containo.us/v1alpha1
kind: TLSOption
metadata:
  name: mintls13
  namespace: traefik2
spec:
  minVersion: VersionTLS13
---
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: ingressroutes.traefik.containo.us
spec:
  group: traefik.containo.us
  version: v1alpha1
  names:
    kind: IngressRoute
    plural: ingressroutes
    singular: ingressroute
  scope: Namespaced
---
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: ingressrouteudps.traefik.containo.us
spec:
  group: traefik.containo.us
  version: v1alpha1
  names:
    kind: IngressRouteUDP
    plural: ingressrouteudps
    singular: ingressrouteudp
  scope: Namespaced
---
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: ingressroutetcps.traefik.containo.us
spec:
  group: traefik.containo.us
  version: v1alpha1
  names:
    kind: IngressRouteTCP
    plural: ingressroutetcps
    singular: ingressroutetcp
  scope: Namespaced

---
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: middlewares.traefik.containo.us
spec:
  group: traefik.containo.us
  version: v1alpha1
  names:
    kind: Middleware
    plural: middlewares
    singular: middleware
  scope: Namespaced
---
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: tlsoptions.traefik.containo.us
spec:
  group: traefik.containo.us
  version: v1alpha1
  names:
    kind: TLSOption
    plural: tlsoptions
    singular: tlsoption
  scope: Namespaced
---
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: tlsstores.traefik.containo.us
spec:
  group: traefik.containo.us
  version: v1alpha1
  names:
    kind: TLSStore
    plural: tlsstores
    singular: tlsstore
  scope: Namespaced
---
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: traefikservices.traefik.containo.us
spec:
  group: traefik.containo.us
  version: v1alpha1
  names:
    kind: TraefikService
    plural: traefikservices
    singular: traefikservice
  scope: Namespaced
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: traefik2-ingress-controller
  namespace: traefik2
rules:
  - apiGroups:
      - ""
    resources:
      - services
      - endpoints
      - secrets
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - extensions
    resources:
      - ingresses
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - extensions
    resources:
      - ingresses/status
    verbs:
      - update
  - apiGroups:
      - traefik.containo.us
    resources:
      - middlewares
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - traefik.containo.us
    resources:
      - ingressroutes
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - traefik.containo.us
    resources:
      - ingressroutetcps
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - traefik.containo.us
    resources:
      - tlsoptions
    verbs:
      - get
      - list
      - watch

---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: traefik2-ingress-controller
  namespace: traefik2
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: traefik2-ingress-controller
subjects:
  - kind: ServiceAccount
    name: traefik2-ingress-controller
    namespace: traefik2
---
apiVersion: v1
kind: Service
metadata:
  name: traefik
  namespace: traefik2
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-resource-group: main-rg-euw
spec:
  ports:
    - protocol: TCP
      name: web
      port: 80
    - protocol: TCP
      name: admin
      port: 8080
    - protocol: TCP
      name: websecure
      port: 443
  selector:
    app: traefik
  type: LoadBalancer
  loadBalancerIP: 51.124.57.24
---
apiVersion: v1
kind: ServiceAccount
metadata:
  namespace: traefik2
  name: traefik2-ingress-controller
---
# grant credentials to all default edit accounts (needed for deployment in Azure DevOps)
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: aggregate-ingressroutes-edit
  namespace: traefik2
  labels:
    rbac.authorization.k8s.io/aggregate-to-admin: "true"
    rbac.authorization.k8s.io/aggregate-to-edit: "true"
rules:
  - apiGroups: ["traefik.containo.us"]
    resources: ["ingressroutes"]
    verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
---
kind: Deployment
apiVersion: apps/v1
metadata:
  namespace: traefik2
  name: traefik
  labels:
    app: traefik
spec:
  replicas: 1
  selector:
    matchLabels:
      app: traefik
  template:
    metadata:
      labels:
        app: traefik
    spec:
      serviceAccountName: traefik2-ingress-controller
      nodeSelector:
        "beta.kubernetes.io/os": linux
      containers:
        - name: traefik
          image: traefik:v2.2
          args:
            - --api.insecure
            - --api.dashboard=true
            - --accesslog
            - --entrypoints.web.Address=:80
            - --entrypoints.websecure.Address=:443
            - --entryPoints.metrics.address=:8082
            - --providers.kubernetescrd
            - --providers.kubernetesingress
            - --providers.kubernetescrd.ingressclass=traefik
            - --providers.kubernetesingress.ingressclass=traefik
            - --certificatesResolvers.default.acme.dnsChallenge
            - --certificatesResolvers.default.acme.dnsChallenge.provider=azure
            - --certificatesresolvers.default.acme.email=hitesh@cloudscale.in
            - --certificatesresolvers.default.acme.storage=acme.json
          ports:
            - name: web
              containerPort: 80
            - name: websecure
              containerPort: 443
            - name: admin
              containerPort: 8080
          env:
            - name: AZURE_CLIENT_ID
              value: "1e9c4589-f873-47db-8100-836014591f98"
            - name: AZURE_CLIENT_SECRET
              value: "E5z7Q~FQBsSSutAcUaSIf8paTtVJaI5OpH60i"
            - name: AZURE_SUBSCRIPTION_ID
              value: "ab8375cd-ba66-46bd-87ed-794853a9caa0"
            - name: AZURE_TENANT_ID
              value: "92d28b9d-5f79-463c-84e6-ea7bc1f51af3"
            - name: AZURE_RESOURCE_GROUP
              value: "main-rg-euw"
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: ingress-dashboard-http
  namespace: traefik2
  annotations:
    kubernetes.io/ingress.class: traefik
spec:
  entryPoints:
    - web
  routes:
    - match: Host(`dashboard.cloudscale.in`)
      kind: Rule
      services:
      - name: traefik
        port: 8080
      middlewares:
      - name: httpsredirect
        namespace: traefik2
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: ingress-dashboard-https
  namespace: traefik2
  annotations:
    kubernetes.io/ingress.class: traefik
spec:
  entryPoints:
    - websecure
  routes:
    - match: Host(`dashboard.cloudscale.in`)
      kind: Rule
      services:
        - name: traefik
          port: 8080
  tls: 
    certResolver: "default"
    options: 
      name: mintls12
      namespace: traefik2
---
# Redirect to https
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: httpsredirect
  namespace: traefik2
spec:
  redirectScheme:
    scheme: https
---
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: stripprefix
  namespace: traefik2
spec:
  stripPrefix:
    prefixes:
      - /letskube
      - /sample
      - /helloworld
      - /sampleapi
      - /portal
      - /tenant-provision
      - /angular
      - /wso
---
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: exposeheaders
  namespace: traefik2
spec:
  headers:
    sslRedirect: true
    contentTypeNosniff: true
    referrerPolicy: "no-referrer"
    forceSTSHeader: true
    stsSeconds: 31536000
    stsIncludeSubdomains: true
    customResponseHeaders:
      dataxh: "test"
      Permissions-Policy: "camera=(), microphone=(), usb=(), geolocation=(), accelerometer=(), screen-wake-lock=(), picture-in-picture=(), gyroscope=(), document-domain=()"
    accessControlMaxAge: -1
    addVaryHeader: true
    accessControlAllowCredentials: true
    accessControlAllowHeaders:
      - "*"
    accessControlAllowOriginList:
      - http://localhost:4200
      - https://cloudscale.in    
    accessControlExposeHeaders:
      - x-correlation-id