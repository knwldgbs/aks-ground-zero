# 1 Backend variables

serviceprinciple_id     = "1e9c4589-f873-47db-8100-836014591f98"
#serviceprinciple_id     = "0f646c98-9af8-4e25-8cac-6fbde484d888"
tenant_id               = "92d28b9d-5f79-463c-84e6-ea7bc1f51af3"
subscription_id         = "ab8375cd-ba66-46bd-87ed-794853a9caa0"

location                = "West Europe"

# Cluster variable
kubernetes_version      = "1.22.6"
ssh_key                 = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9Qcso3kOqM7qhDBXoBtJbEWbzBvwrnoQUeEAK+Wx7UFx0C3d3DGvdRjXSfhfjkLeH2meByfRlU8gvG0xOQ1FzsG5QqT2vb+r/Q6/eUQyAkHqNzIR1qfkgRxMpoxbK2KhC3Q3JkK0E29Z1LdbBrZBSswWkAj3xyqTIRpFZ+le59stN2aLYn9Pek5ya3FViYnnbjUMJHbKbttnSzaAYHHlDLIKev70mcO1+ZYXeBRYvZlT2aNtSrw7OzScole09tuCA32jjWEKzCgmsc1BiKjuS51/sambTrFOAXb52uueaILT8AjPSLjs/Z+lsylC+mm6HTrdZMjAC/UrvCzyDx7ln"
