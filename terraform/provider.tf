terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "=2.73.0"
    }
  }

   backend "azurerm" {
         storage_account_name = "traformstate"
         container_name       = "terraformstate"
         key                  = "terraform.tfstate"
         access_key           = "iXzax8KqApgA8ZUi/tDiPnWFlN6A/SHeeOXlbevMK5gRBqd7a/8uuMazxj8gapkUSwQ9yguotFsOFeeZhoyPgA=="
     }

}

provider "azurerm" {
 

  subscription_id = var.subscription_id
  client_id       = var.serviceprinciple_id
  client_secret   = var.serviceprinciple_key
  tenant_id       = var.tenant_id

  features {}
}

module "cluster" {
  source                = "./modules/cluster/"
  serviceprinciple_id   = var.serviceprinciple_id
  serviceprinciple_key  = var.serviceprinciple_key
  ssh_key               = var.ssh_key
  location              = var.location
  kubernetes_version    = var.kubernetes_version    
}
