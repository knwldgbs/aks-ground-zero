# Step by step provisioning of AKS #


### Reference material ###
* https://github.com/marcel-dempers/docker-development-youtube-series 
* https://www.youtube.com/watch?v=bHjS4xqwc9A
* https://github.com/marcel-dempers/docker-development-youtube-series/blob/master/kubernetes/cloud/azure/getting-started.md


### Steps - Terraform Setup ###
* https://docs.microsoft.com/en-us/azure/developer/terraform/create-k8s-cluster-with-tf-and-aks 
* Steps:
    - Az login (login with decos.com account)
    - az account set -s ab8375cd-ba66-46bd-87ed-794853a9caa0
    - Create Azure Stroage account in Azure and Get Storage access key
    - $env:$STORAGE_ACCESS_KEY = iXzax8KqApgA8ZUi/tDiPnWFlN6A/SHeeOXlbevMK5gRBqd7a/8uuMazxj8gapkUSwQ9yguotFsOFeeZhoyPgA=={KEY_GOES_HERE}
    - az storage container create -n terraformstate --account-name traformstate --account-key $STORAGE_ACCESS_KEY

### Steps - Environment variables ###
####$TENANT_ID="92d28b9d-5f79-463c-84e6-ea7bc1f51af3"
###$SUBSCRIPTION_ID="ab8375cd-ba66-46bd-87ed-794853a9caa0"
#######$RESOURCE_GROUP="aks-getting-started"
###$SERVICE_PRINCIPAL="0f646c98-9af8-4e25-8cac-6fbde484d888" //ID of App registration Id
$SERVICE_PRINCIPAL_SECRET="E5z7Q~FQBsSSutAcUaSIf8paTtVJaI5OpH60i"
####$SSH_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9Qcso3kOqM7qhDBXoBtJbEWbzBvwrnoQUeEAK+Wx7UFx0C3d3DGvdRjXSfhfjkLeH2meByfRlU8gvG0xOQ1FzsG5QqT2vb+r/Q6/eUQyAkHqNzIR1qfkgRxMpoxbK2KhC3Q3JkK0E29Z1LdbBrZBSswWkAj3xyqTIRpFZ+le59stN2aLYn9Pek5ya3FViYnnbjUMJHbKbttnSzaAYHHlDLIKev70mcO1+ZYXeBRYvZlT2aNtSrw7OzScole09tuCA32jjWEKzCgmsc1BiKjuS51/sambTrFOAXb52uueaILT8AjPSLjs/Z+lsylC+mm6HTrdZMjAC/UrvCzyDx7ln"

### Service principle ###
az role assignment create --assignee $SERVICE_PRINCIPAL --scope "/subscriptions/$SUBSCRIPTION_ID" --role Contributor

### Terraform commands ### 
> Init
terraform init
> Plan 
terraform plan -var serviceprinciple_key="$SERVICE_PRINCIPAL_SECRET" -out myplan.tfplan
> Apply 
terraform apply -var serviceprinciple_key="$SERVICE_PRINCIPAL_SECRET" 
> Destroy 
terraform destroy -var serviceprinciple_key="$SERVICE_PRINCIPAL_SECRET" 

================================================================================
### Connect cluster ### 

az aks get-credentials --resource-group cloudscale-rg --name cloudscale-aks --admin

================================================================================
### Traefik ### 
Apply steps from > D:\git\aks-ground-zero\Traefik2.5\readme.md


================================================================================
* HTTP: Mape sure to give rights to DNS Zone Contributor 

"DNS Zone Contributor" role refer to image DNS Zone > d:/git/terraform/azure/01-basic/DNS ZONE-Contributor.JPG (git)


## >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>...
az aks get-versions --location westeurope -o table

## >>>>>>>>>>>>>>>>>>>>>>>>>>>
ssh-keygen -t rsa -b 4096 -N "VeryStrongSecret123!" -C "your_email@example.com" -q -f  C:/Users/hitesh.patadia/.ssh/id_rsa

cp C:/Users/hitesh.patadia/.ssh/id_rsa* .
