* Install certificate manager
> Just for reference: https://getbetterdevops.io/k8s-ingress-with-letsencrypt/
> Just for reference: https://cert-manager.io/v1.3-docs/installation/kubernetes/
> https://cert-manager.io/v1.3-docs/installation/kubernetes/#installing-with-helm
> helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.8.0 --set installCRDs=true

* Create staging certificate
kubectl apply -f .\letsencrypt-staging.yaml

* Verfiy production
kubectl apply -f .\letsencrypt-production.yaml